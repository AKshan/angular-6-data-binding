import { Component, OnInit } from '@angular/core';
import { DataService} from '../data.service';
import {observable} from 'rxjs/internal-compatibility';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  postuser$: Object;
  constructor(private data: DataService) { }

  ngOnInit() {
    this.data.postUsers().subscribe(
      data => this.postuser$ = data
    );
  }

}
